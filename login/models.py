from django.db import models

# Create your models here.

from django.db import models
from django.contrib.auth.models import User
import django.utils.timezone as timezone
from django.contrib.auth.models import AbstractUser

# Create your models here.

class Patient(User):
    hasto_change_password = models.BooleanField(default=False)
    phone = models.CharField(max_length=17, blank=False, null=True)
    name = models.CharField(max_length=50, blank=False, null=True)
    birthday = models.DateField(blank=False, null=True)
    height = models.DecimalField(max_digits=19,
                                 decimal_places=16,
                                 blank=False,
                                 null=True)
    gender = models.CharField(max_length=1, blank=False, null=True)
    fcm_id = models.CharField(max_length=50, blank=False, null=True)
    address = models.CharField(max_length=50, blank=False, null=True)
    weight = models.DecimalField(max_digits=19,
                                 decimal_places=16,
                                 blank=False,
                                 null=True)
    fb_id = models.CharField(max_length=50, blank=False, null=True)
    status = models.CharField(max_length=50, default='Normal')
    group = models.CharField(max_length=50, blank=False, null=True)
    unread_records = models.DecimalField(max_digits=10,
                                             decimal_places=0,
                                             default=0)

    verified = models.CharField(max_length=1, default='0')
    privacy_policy = models.CharField(max_length=1, default='0')
    must_change_password = models.CharField(max_length=1, default='0')
    badge = models.DecimalField(max_digits=15, decimal_places=0, default=0)
    login_times = models.DecimalField(max_digits=15,
                                      decimal_places=0,
                                      default=0)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)
    invite_code = models.CharField(null=True, blank=True, max_length=20)

    def __str__(self):
        return self.username
