from django.shortcuts import render

# Create your views here.

from django.shortcuts import render
from django.shortcuts import render
from login.models import *
from django.http import HttpResponse, JsonResponse, QueryDict
from django.views.decorators.csrf import csrf_exempt

# API(1) register
# 註冊


@csrf_exempt
def register(request):

    if request.method == 'POST':

        data = request.POST
        f = RegisterForm(data)

        if f.is_valid():
            account = f.cleaned_data['account']
            phone = f.cleaned_data['phone']
            email = f.cleaned_data['email']
            password = f.cleaned_data['password']

            Patient.objects.create_user(
                username=account,
                email=email,
                phone=phone,
                password=password,
                is_active=False)

            status = "0"
            return JsonResponse({'status': status})
    else:
        status = "1"
    return JsonResponse({'status': status})


# API(2) auth
# 登入
@csrf_exempt
def user_auth(request):
    if request.method == 'POST':

        username = request.POST['account']
        password = request.POST['password']
        u = Patient.objects.get(username=username)

        if u:
            if not u.is_active:
                status = "2"
            elif u.is_active:

                user = auth.authenticate(username=username, password=password)

                if user:

                    auth.login(request, user)
                    s = Session.objects.all()[0]
                    request.session['account'] = username
                    token = s.session_data
                    status = "0"

                    return JsonResponse({'status': status, 'token': token})
                else:
                    status = "1"

    return JsonResponse({'status': status})


def hello_world(request):
    return HttpResponse("Hello World Version 2 !")
